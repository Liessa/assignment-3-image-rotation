#include "image.h"

#ifndef ROTATE_H
#define ROTATE_H

struct image rotate_by_angle(struct image const source, int16_t const angle);

#endif

#include  <stdint.h>

#ifndef IMAGE_H
#define IMAGE_H

struct pixel { uint8_t b, g, r; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};


void cleanup_image_memory(struct image *image);

uint64_t get_pixel_index(const uint32_t width, const uint32_t row, const uint32_t column);

struct image create_empty_image(const uint32_t width, const uint32_t height);

void cleanup_image(struct image *image);

#endif //IMAGE_H

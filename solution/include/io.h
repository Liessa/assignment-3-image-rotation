#include "file.h"
#include "image.h"
#include "stdint.h"
#ifndef IO_H
#define IO_H
extern char* angle_status_names[3];
extern char* input_args_status_names[2];


typedef enum input_args_status  {
    ARGS_VALID,
    ARGS_ERROR
} input_args;

typedef enum angle_status {
    ANGLE_VALID,
    ANGLE_FORMAT_ERROR,
    ANGLE_VALUE_ERROR
} angle_status;

angle_status read_angle(char* angel, int16_t* result);
input_args validate_input_count(const uint16_t argc);
void handle_file_error(FILE *file, const char *errorMessage, struct image *imageData);

#endif //IO

#include <stdio.h>
#ifndef FILE_H
#define FILE_H


enum file_interaction {
    INTERACTION_OK = 0,
    INTERACTION_ERROR,
};
FILE*  open_file_read(const char* name);
FILE*  file_write(const char* name);
enum file_interaction close_file(FILE* file);
#endif //FILE_H



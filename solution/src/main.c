#include "bmp.h"
#include "file_handler.h"
#include "image.h"
#include "io.h"
#include "rotate.h"



int main(int argc, char **argv) {
    if (validate_input_count(argc) != ARGS_VALID) {
        return 1;
    }
    char* output_image = argv[2];
    int16_t angle = 0;
    angle_status read_angle_status = read_angle(argv[3], &angle);

    if (read_angle_status != ANGLE_VALID) {
        return EXIT_FAILURE;
    }


    FILE * in = open_file_read(argv[1]);
    if (in == NULL) {
        return EXIT_FAILURE;
    }

    struct image loadedImage = {0};
    struct bmp_image bmp_image = {.bmp_file = in, .bmp_image = &loadedImage};

    if (from_bmp(bmp_image) != READ_OK) {
        close_file(in);
        return EXIT_FAILURE;
    }
    fclose(in);


    struct image newImage = rotate_by_angle(loadedImage, angle);
    if(newImage.height==0 && newImage.width==0){
        return EXIT_FAILURE;
    }

    FILE *out = file_write(output_image);
    struct bmp_image new_bmp_image = {.bmp_file = out, .bmp_image = &newImage};
    if (out == NULL) {
        close_file(out);
        return EXIT_FAILURE;
    }
    if (    to_bmp(new_bmp_image) != WRITE_OK) {
        close_file(out);
        return EXIT_FAILURE;
    }

    cleanup_image_memory(&loadedImage);
    cleanup_image_memory(&newImage);
    fclose(out);

    return EXIT_SUCCESS;
}

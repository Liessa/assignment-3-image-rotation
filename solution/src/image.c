#include "image.h"
#include <stdlib.h>

void cleanup_image_memory(struct image *image) {
    if (image->data != NULL) {
        free(image->data);
        image->data = NULL;
    }
}

void cleanup_image(struct image *image) {
    if (image->data) {
        free(image->data);
    }
    free(image);
}

uint64_t get_pixel_index(const uint32_t width, const uint32_t row, const uint32_t column) {
    return row * width + column;

}
static uint64_t get_size(const uint32_t width, const uint32_t height) {
    return width * height;
}
struct image create_empty_image(const uint32_t width, const uint32_t height) {
    struct pixel* pixels = (struct pixel*) malloc(get_size(width, height) * sizeof(struct pixel));
    struct image img = {width, height, pixels};
    return img;

}


#include "image.h"
#include "io.h"
#include "stdio.h"
#include <stdlib.h>



char* angle_status_names[3] = {
        "The angle are valid",
        "Wrong angle format",
        "Wrong angle value"
};
char* input_args_status_names[2] = {
        "The arguments are valid",
        "The arguments are not valid"
};

input_args validate_input_count(const uint16_t argc) {
    if (argc != 4) {
        return ARGS_ERROR;
    }
    return ARGS_VALID;
}

angle_status validate_angle(char* string, int16_t* angle) {
    char* end;
    int16_t result = (int16_t) strtol(string, &end, 10);
    if (end == string) {
        return ANGLE_FORMAT_ERROR;
    }
    if (result >= INT16_MAX || result <= INT16_MIN) {
        return ANGLE_FORMAT_ERROR;
    }
    *angle = result;
    return ANGLE_VALID;
}
angle_status is_permissible_angle(int16_t corner) {
    int16_t permissible_angel[7] = {0, 90, -90, 180, -180, 270, -270};
    for (size_t i = 0; i < 7; i++) {
        if (permissible_angel[i] == corner) {
            return ANGLE_VALID;
        }
    }
    return ANGLE_VALUE_ERROR;
}
angle_status read_angle(char* angel, int16_t* result) {
    int16_t temp_result;
    if (validate_angle(angel, &temp_result) == ANGLE_FORMAT_ERROR) return ANGLE_FORMAT_ERROR;
    if (is_permissible_angle(temp_result) == ANGLE_VALUE_ERROR) return ANGLE_VALUE_ERROR;
    *result = temp_result;
    return ANGLE_VALID;
}

void handle_file_error(FILE *file, const char *errorMessage, struct image *imageData) {
    perror(errorMessage);
    if (file != NULL) {
        fclose(file);
    }
    if (imageData->data != NULL) {
        cleanup_image_memory(imageData);
    }
}


#include "file_handler.h"
#include <stdio.h>

char* file_proc_status_names[2] = {
        "File processing was successful",
        "An error occurred while processing the file"
};


file_status read_file(char* filename, FILE** f) {
    if ((*f = fopen(filename, "rb")) == NULL) {
        return FILE_ERROR;
    }
    return FILE_OK;
}

file_status write_file(char* filename, FILE** f) {
    if ((*f = fopen(filename, "wb")) == NULL) {
        return FILE_ERROR;
    }
    return FILE_OK;
}




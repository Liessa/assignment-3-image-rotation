#include "file.h"



FILE*  open_file_read(const char* name){
    FILE* opened_file = fopen(name, "rb");
    return opened_file;
}

FILE * file_write (const char* name){
    FILE* file_to_write = fopen(name, "wb");
    return file_to_write;
}

enum file_interaction close_file(FILE* file) {
    if (fclose(file)){
        return INTERACTION_ERROR;
    }
    return INTERACTION_OK;
}

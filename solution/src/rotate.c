#include "image.h"
#include <stdio.h>


static uint64_t get_rotated_pixel_index(uint32_t width, uint32_t height, uint32_t row, uint32_t column, const uint8_t times) {
    for (uint8_t i = 0; i < times; i++) {
        uint32_t tmp_row = row;
        row = width - column - 1;
        column = tmp_row;
        uint32_t tmp_width = width;
        width = height;
        height = tmp_width;

    }

    return get_pixel_index(width, row, column);
}

static struct image rotate(struct image const source, const uint8_t times) {
    if (source.data == NULL) {
        return (struct image) {0};
    }
    uint64_t rotated_width = times % 2 == 0 ? source.width : source.height;
    uint64_t rotated_height = times % 2 == 0 ? source.height : source.width;
    struct image rotated_img = create_empty_image(rotated_width, rotated_height);
    if (rotated_img.data == NULL) {
        return (struct image) {0};
    }
    for (uint32_t i = 0; i < source.height; i++) {
        for (uint32_t j = 0; j < source.width; j++) {
            rotated_img.data[get_rotated_pixel_index(source.width, source.height, i, j, times)] = source.data[get_pixel_index(source.width, i, j)];
        }
    }
    return rotated_img;
}

struct image rotate_by_angle(struct image const source, int16_t const angle) {
    int16_t pos_angle = angle;
    if (pos_angle < 0) {
        pos_angle += 360;
    }
    return rotate(source, pos_angle / 90);
}

#include "image.h"
#include "bmp.h"

#define ALIGNMENT_BYTE 4
#define BMP_CODE 0x4D42

uint32_t calc_padding(const uint32_t width){
    return (ALIGNMENT_BYTE - ((width * sizeof(struct pixel)) % ALIGNMENT_BYTE)) % ALIGNMENT_BYTE;
}

static enum read_status read_bmp(struct image *img, FILE *file){
    uint32_t padding = calc_padding(img->width);
    size_t pixel_size = sizeof(struct pixel);
    for (uint64_t i = 0; i < img->height; ++i) {
        for (uint64_t j = 0; j < img->width; ++j) {
            if (fread(&img->data[i * img->width + j], 1, pixel_size, file) != pixel_size) {
                free(img->data);
                return READ_INVALID_BITS;
            }

        }
        if(fseek(file, (long) padding, SEEK_CUR)!=0){
            return READ_FAILURE;

        }
    }

    return READ_OK;
}

enum read_status from_bmp( struct bmp_image bmp_image){
    FILE* in = bmp_image.bmp_file;
    struct image *img = bmp_image.bmp_image;


    struct bmp_header header = {0};
    size_t read_bytes_num = fread(&header, sizeof(struct bmp_header), 1, in);
    if (!read_bytes_num){
        return READ_INVALID_HEADER;
    }

    if (header.bfType != BMP_CODE) {
        return READ_INVALID_SIGNATURE;
    }

    img->width = (uint64_t)header.biWidth;
    img->height = (uint64_t)header.biHeight;
    img->data = malloc(sizeof(struct pixel) * img->width * img->height);
    if (!img->data) {
        return READ_INVALID_BITS;
    }

    if(fseek(in, header.bOffBits, SEEK_SET) != 0){
        return READ_FAILURE;
    }


    if (read_bmp(img, in) != READ_OK){
        return READ_INVALID_BITS;
    }
    return READ_OK;
}

static enum write_status write_header(struct bmp_header header, FILE* out){
    if (!fwrite(&header, sizeof(struct bmp_header), 1, out)) {
        return WRITE_ERROR;
    }
    return WRITE_OK;
}

static enum write_status write_bmp(struct image* image, FILE* file){
    for (uint32_t y = 0; y < image -> height; y++) {
        if (fwrite(&image -> data[image -> width * y], sizeof(struct pixel), image -> width, file) != image -> width) {
            return WRITE_ERROR;
        }
        if(fseek(file, calc_padding(image->width), SEEK_CUR) !=0){
            return (enum write_status) READ_FAILURE;
        }
    }

    return WRITE_OK;
}
struct bmp_header create_header(struct image* image, uint32_t padding){
    struct bmp_header header = (struct bmp_header) {
            .bfType = 0x4D42,
            .bfileSize = sizeof(struct bmp_header) + image->width * image->height * sizeof(struct pixel)
                         + padding * image->height,
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = image->width * image->height * sizeof(struct pixel) + padding * image->height,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
    return header;
}
enum write_status to_bmp( struct bmp_image bmp_image){
    struct image* image = bmp_image.bmp_image;
    FILE* file = bmp_image.bmp_file;

    uint32_t padding = calc_padding(image->width);

    struct bmp_header header = create_header(image, padding);
    if (write_header(header, file) != WRITE_OK){
        return WRITE_ERROR;
    }

    if (write_bmp(image, file) != WRITE_OK){
        return WRITE_ERROR;
    }

    return WRITE_OK;
}


